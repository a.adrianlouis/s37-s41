const express = require("express")
const router = express.Router()
const CourseController = require("../controllers/CourseController")
const auth = require("../auth")

router.get("/", (request, response) => {
	CourseController.getAllCourses().then(result => response.send(result))
})

// Get all ACTIVE courses
router.get("/active", (request, response) => {
	CourseController.getAllActive().then(result => response.send(result))
})

// Create new course
router.post("/", auth.verify, (request, response) => {
	CourseController.createCourse(request.body).then(result => response.send(result))
})

// Get single course
router.get("/:id", (request, response) => {
	CourseController.getCourse(request.params.id).then(result => response.send(result))
})

// Update details of existing course
router.patch("/:id/update", auth.verify, (request, response) => {
	CourseController.updateCourse(request.params.id, request.body).then(result => response.send(result))
})

// Archive a course
router.patch("/:id/archive", auth.verify, (request, response) => {
	CourseController.archiveCourse(request.params.id).then(result => response.send(result))
})

// ACTIVITY 
// Create a route and controller function for getting ALL courses (regardless if active or not) and for archiving a course
// - For archiving a course, the URL endpoint must be '/:id/archive'
// - Also, to archive the course, you must pass the course_id to the controller function and use that ID to update the course's isActive field ONLY.
// - For getting ALL courses, the URL endpoint must be '/'

module.exports = router
