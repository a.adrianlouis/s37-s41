const Course = require("../models/Course")

// Get all ACTIVE courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(active_courses => {
		return active_courses
	})
}

// Create new course
module.exports.createCourse = (request_body) => {
	let new_course = new Course({
		name: request_body.name,
		description: request_body.description,
		price: request_body.price 
	})

	return new_course.save().then((created_course, error) => {
		if(error){
			return error 
		}

		return {
			message: "Course created successfully!",
			data: created_course
		}
	})
}

// Get single course
module.exports.getCourse = (course_id) => {
	return Course.findById(course_id).then(result => {
		return result 
	})
}

// Update existing course
module.exports.updateCourse = (course_id, new_content) => {
	let updated_course = {
		name: new_content.name,
		description: new_content.description,
		price: new_content.price 
	}

	return Course.findByIdAndUpdate(course_id, updated_course).then((modified_course, error) => {
		if(error){
			return error 
		}

		return {
			message: "Course updated successfully!",
			data: modified_course
		}
	})
}

// Get all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then((courses, error) => {
		if(error){
			return error 
		}

		return courses
	})
}

// Archive course
module.exports.archiveCourse = (course_id) => {
	return Course.findById(course_id).then((course, error) => {
		if(error){
			return error
		}

		course.isActive = false

		return course.save().then((archived_course, error) => {
			if(error){
				return error 
			}

			return archived_course
		})
	})
}
